const {JSDOM} = require("jsdom");
const fsExtra = require("fs-extra");
const shell = require("shelljs");
const pretty = require("pretty");
const { merge } = require("jquery");
const argv = require("yargs").argv;
const {path: pathIndexFile} = argv;
const outDirectory = `./dist`;
const inpDirectory = pathIndexFile.split("/index.html")[0];


const term = (cmd) => {
    return new Promise((resolve,reject) => {
        console.log("\n--$>", cmd);
        if(shell.exec(cmd).code === 0) resolve(true);
        else reject(false);
    });
}

const mergeContentInFiles = (lstFilePaths, baseDir=inpDirectory) => {
    let merged = '';

    for(let i in lstFilePaths){
        const path = `${baseDir}/${lstFilePaths[i]}`;
        const ext = path.split(".").slice(-1)[0];
        const content = (fsExtra.readFileSync(path)).toString();
        merged += (ext === 'js' ? `(function(){${content}})();` : content) + "\n\n\n";
    }

    return merged;
}

const run = async () => {
    try{
        //create output directory
        if(fsExtra.existsSync(outDirectory)) fsExtra.emptyDirSync(outDirectory);
        else fsExtra.ensureDirSync(outDirectory);

        //load the content from index.html, turn it into a DOM model
        const content = fsExtra.readFileSync(pathIndexFile);
        const strContent = content.toString();
        const dom = new JSDOM(strContent);
        const $ = (require("jquery"))(dom.window);
        const css = new Set();
        const js = new Set();
        
        //get all the link tags in the <head>
        const linkTags = $("head link");
        const scriptTags = $("body script");
        
        //clean out link tags, save unique refs to css files
        for(let i = 0; i < linkTags.length; i++){
            const {href} = linkTags[i];
            const ext = href.split(".").slice(-1)[0];
            if(ext.trim().toLowerCase() === 'css' && href && href.trim().length > 0) css.add(href);
            linkTags[i].remove();
        }

        //clean out script tags, save unique refs to css files
        for(let i = 0; i < scriptTags.length; i++){
            const {src} = scriptTags[i];
            if(src && src.trim().length > 0) {
                js.add(src);
                scriptTags[i].remove();
            }
        }

        //merge css content in head
        const cssContent = mergeContentInFiles(Array.from(css));
        fsExtra.writeFileSync(`${outDirectory}/css-merged.css`, cssContent);
        dom.window.document.head.innerHTML += `<link rel='stylesheet' href='/css-merged.css'>\n`;

        //merge js content in body
        const jsContent = mergeContentInFiles(Array.from(js));
        fsExtra.writeFileSync(`${outDirectory}/js-merged.js`, jsContent);
        dom.window.document.body.innerHTML += `<script src='/js-merged.js'></script>\n`;
        
        //write the final html
        const finalHtmlContent = dom.window.document.documentElement.outerHTML.split("\n").join("");
        fsExtra.writeFileSync(`${outDirectory}/index.html`, pretty(finalHtmlContent));

        //copy over fonts and images
        const dirs = ["fonts", "images"];
        for(let i in dirs){
            const subdir = dirs[i];
            const fullPathIn = `${inpDirectory}/${subdir}`;
            const fullPathOut = `${outDirectory}/${subdir}`;
            await term(`cp -r ${fullPathIn} ${fullPathOut}`);
            console.log(`--> copying over: ${subdir}`);
        }
    }
    catch(err){
        console.error(err);
    }
}

run().then(console.log).catch(console.error);